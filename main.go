package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

// Configuration data for events
type EventConfiguration struct {
	Type       string `json:"type"`
	Command    string `json:"command"`
	Branch     string `json:"branch"`
	AuthSecret string `json:"auth_secret"`
}

// Incoming event structures
type GitlabPipelineAttributes struct {
	Branch string `json:"ref"`
	Status string `json:"status"`
}

type GitlabEvent struct {
	ObjectKind       string                   `json:"object_kind"`
	Branch           string                   `json:"ref"`
	ObjectAttributes GitlabPipelineAttributes `json:"object_attributes"`
}

type GithubPushEvent struct {
	Branch string `json:"ref"`
}

// Configuration
var conf map[string]EventConfiguration

func LoadConfiguration(configPath string) {
	curlog := logrus.WithField("path", configPath)

	confFile, err := os.Open(configPath)
	if err != nil {
		curlog.WithField("error", err).Fatal("Could not open configuration")
	}
	defer confFile.Close()

	decoder := json.NewDecoder(confFile)
	err = decoder.Decode(&conf)
	if err != nil {
		curlog.WithField("error", err).Fatal("Could not read configuration")
	}

	// Validate configuration
	for k, ev := range conf {
		switch ev.Type {
		case "gitlab_pipeline":
		case "github_push":
		case "gitlab_push":
			//ok
		default:
			curlog.WithField("event", k).Fatal("Non-recognized event type")
		}
	}
}

// Task execution
func doExecute(event EventConfiguration, evlog logrus.FieldLogger) {
	task := exec.Command(event.Command)
	err := task.Run()
	if err != nil {
		evlog.WithField("error", err).Error("Problem running script")
	}
}

// Event handling
func handleGitlabPipelineEvent(event EventConfiguration, evlog logrus.FieldLogger, w http.ResponseWriter, r *http.Request) {
	// Validate secret header
	if r.Header.Get("X-Gitlab-Token") != event.AuthSecret {
		evlog.Info("Unauthenticated request")
		w.WriteHeader(401)
		return
	}

	// Decode json
	body := json.NewDecoder(r.Body)
	var eventdata GitlabEvent
	err := body.Decode(&eventdata)
	if err != nil {
		evlog.WithField("error", err).Error("Problem decoding event data")
		// Return success to gitlab since them retrying wont help anyone.
		w.WriteHeader(204)
		return
	}

	// Check whether this is a pipeline success event
	if eventdata.ObjectKind == "pipeline" && eventdata.ObjectAttributes.Status == "success" {
		evlog.Info("Detected pipeline success, running task")
		// Spin off command execution to separate thread to ensure swift response to gitlab
		go doExecute(event, evlog)
	}

	// Signal success
	w.WriteHeader(204)
}

func handleGitlabPushEvent(event EventConfiguration, evlog logrus.FieldLogger, w http.ResponseWriter, r *http.Request) {
	// Validate secret header
	if r.Header.Get("X-Gitlab-Token") != event.AuthSecret {
		evlog.Info("Unauthenticated request")
		w.WriteHeader(401)
		return
	}

	// Decode json
	body := json.NewDecoder(r.Body)
	var eventdata GitlabEvent
	err := body.Decode(&eventdata)
	if err != nil {
		evlog.WithField("error", err).Error("Problem decoding event data")
		// Return success to gitlab since them retrying wont help anyone.
		w.WriteHeader(204)
		return
	}

	if eventdata.Branch == event.Branch {
		evlog.Info("Detected push, running task")
		go doExecute(event, evlog)
	}

	// Signal success
	w.WriteHeader(204)
}

func handleGithubPushEvent(event EventConfiguration, evlog logrus.FieldLogger, w http.ResponseWriter, r *http.Request) {
	// Read request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		evlog.WithField("err", err).Error("Reading request failed")
		w.WriteHeader(204)
		return
	}

	// Validate hmac
	digest := r.Header.Get("X-Hub-Signature")
	remoteHmac, err := hex.DecodeString(digest[5:]) // strip off prefix sha1=
	if err != nil {
		evlog.WithField("error", err).Error("Invalid signature encoding")
		w.WriteHeader(204)
		return
	}
	mac := hmac.New(sha1.New, []byte(event.AuthSecret))
	mac.Write(body)
	referenceHmac := mac.Sum([]byte{})
	if !hmac.Equal(remoteHmac, referenceHmac) {
		evlog.Info("Unauthenticated request")
		w.WriteHeader(401)
		return
	}

	// Decode body
	var evdata GithubPushEvent
	err = json.Unmarshal(body, &evdata)
	if err != nil {
		evlog.WithField("error", err).Error("Problem decoding event data")
		// Return success to stop remote from retrying this
		w.WriteHeader(204)
		return
	}

	// Check event restrictions
	if evdata.Branch == event.Branch {
		evlog.Info("Detected push, running task")
		go doExecute(event, evlog)
	}

	// Signal success
	w.WriteHeader(204)
}

func handleEvent(w http.ResponseWriter, r *http.Request) {
	eventID := chi.URLParam(r, "eventID")
	evlog := logrus.WithField("event", eventID)

	// Fetch event data
	event, ok := conf[eventID]
	if !ok {
		evlog.Info("Unknown event")
		w.WriteHeader(404)
		return
	}

	// Run handler
	switch event.Type {
	case "gitlab_pipeline":
		handleGitlabPipelineEvent(event, evlog, w, r)
	case "github_push":
		handleGithubPushEvent(event, evlog, w, r)
	case "gitlab_push":
		handleGitlabPushEvent(event, evlog, w, r)
	default:
		evlog.WithField("type", event.Type).Error("Unrecognized event type (shouldn't trigger)")
		// Return success to stop remote from retrying
		w.WriteHeader(204)
		return
	}
}

func main() {
	if len(os.Args) < 2 {
		logrus.Fatal("Missing configuration")
	}
	if len(os.Args) > 2 {
		logrus.Fatal("Too many arguments")
	}
	LoadConfiguration(os.Args[1])

	r := chi.NewRouter()

	r.Post("/{eventID}", handleEvent)

	logrus.Info("Server succesfully started")

	logrus.WithField("error", http.ListenAndServe(":8085", r)).Fatal("Server crashed")
}
