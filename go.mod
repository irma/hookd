module github.com/privacybydesign/hookd

go 1.14

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
)
